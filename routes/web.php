<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\KritikController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

// CRUD cast
// Create
// form cast
Route::get('/cast/create',[CastController::class, 'create']);
//kirim ke DB
Route::post('/cast', [CastController::class, 'store']);

// Read
//tampil
Route::get('/cast', [CastController::class, 'index']);
// detail berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// Update
// form update kategori
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// update DB berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// Delete
// hapus berdasarkan id
Route::delete('/cast/{cast_id}', [CastController::class, 'delete']);

// CRUD film
Route::resource('film', FilmController::class);

// kritik
Route::Post('/kritik/{film_id}', [KritikController::class, 'store']);

// CRUD genre
Route::resource('genre', GenreController::class);

// role
Route::resource('role', RoleController::class);
Auth::routes();