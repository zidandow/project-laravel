<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kritik;
use Illuminate\Support\Facades\Auth;

class KritikController extends Controller
{
    public function store($film_id, Request $request){

        $request->validate([
            'content' => 'required',
            'point' => 'required'
        ]);

        Kritik::create([            
            'user_id' => Auth::id(),
            'film_id' => $film_id,
            'content' => $request->input('content'),
            'point' => $request->input('point')
        ]);

        return redirect('/film/'.$film_id);
    }
}