<?php

namespace App\Http\Controllers;

use App\Models\genre;
use App\Models\film;
use Illuminate\Http\Request;
use File;

class FilmController extends Controller
{
     /**
     * Instantiate a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'edit']);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $film = film::all();

        return view('film.index', ['film'=>$film]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $genre = genre::all();

        return view('film.create', ['genre' => $genre]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|mimes:jpg,png,jpeg|max: 2048 ',
            'genre_id' => 'required',
        ]);

        $posterName = time().'.'.$request->poster->extension();

        $request->poster->move(public_path('poster'), $posterName);

        film::create([
            'judul' => $request->input('judul'),
            'ringkasan' => $request->input('ringkasan'),
            'tahun' => $request->input('tahun'),
            'poster' => $posterName,
            'genre_id' => $request->input('genre_id'),
        ]);

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $film=film::find($id);

        return view('film.detail', ['film'=>$film]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $film=film::find($id);
        $genre = genre::all();

        return view('film.edit', ['film'=>$film, 'genre'=>$genre]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'mimes:jpg,png,jpeg|max: 2048 ',
            'genre_id' => 'required',
        ]);

        $film = film::find($id);

        if($request->has('poster')){
            $path = 'poster/';

            File::delete($path. $film->poster);
            
            $posterName = time().'.'.$request->poster->extension();

            $request->poster->move(public_path('poster'), $posterName);

            $film->poster = $posterName;
        }

        $film->judul = $request->input('judul');
        $film->ringkasan = $request->input('ringkasan');
        $film->tahun = $request->input('tahun');
        $film->genre_id = $request->input('genre_id');

        $film->save();

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $film = Film::find($id);

        File::delete('poster/'. $film->poster);
        
        $film->delete();

        return redirect('/film');
    }
}