<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\role;
use App\Models\film;
use App\Models\Cast;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'edit']);
    }
    
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $role = role::all();

        return view('role.index', ['role'=>$role]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $film = film::all();
        $Cast = Cast::all();

        return view('role.create', ['film' => $film, 'Cast' => $Cast]);
        
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'film_id' => 'required',
            'cast_id' => 'required',
            'nama' => 'required'
        ]);

        role::create([
            'film_id' => $request->input('film_id'),
            'cast_id' => $request->input('cast_id'),
            'nama' => $request->input('nama'),
        ]);

        return redirect('/role');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $role=role::find($id);

        return view('role.detail', ['role'=>$role]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $role=role::find($id);
        $film = film::all();
        $Cast = Cast::all();

        return view('role.edit', ['role'=>$role, 'film'=>$film, 'cast'=>$Cast]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'film_id' => 'required',
            'cast_id' => 'required',
            'nama' => 'required'
        ]);

        $role = role::find($id);

        $role->film_id = $request->input('film_id');
        $film->cast_id = $request->input('cast_id');
        $film->nama = $request->input('nama');

        $role->save();

        return redirect('/role');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $role = role::find($id);
        
        $role->delete();

        return redirect('/role');
    }
}