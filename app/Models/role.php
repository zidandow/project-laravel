<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class role extends Model
{
    use HasFactory;

    
    protected $table="role";
    protected $fillable=['film_id', 'cast_id', 'nama'];

    public function film(): BelongsTo{

        return $this->belongsTo(film::class, 'film_id');
    }

    public function cast(): BelongsTo{

        return $this->belongsTo(cast::class, 'cast_id');
    }
}