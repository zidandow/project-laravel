@extends('layout.master')

@section('judul')
    Halaman Daftar Peran
@endsection

@push('script')
    <script src="{{asset('/AdminLTE-3.2.0/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('/AdminLTE-3.2.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
      $(function () {
        $("#example1").DataTable();
      });
    </script>
@endpush

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

    <div class="card">
        <div class="card-body">
            <table class="table table-hover" id="example1">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Film</th>
                        <th scope="col">Cast</th>
                        <th scope="col">Nama Peran</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($role as $key => $item)
                    <tr>
                    <th scope="row">{{$item->id}}</th>
                    <td><a href="/role/{{$item->id}}" class="text-dark">{{$item->film->judul}}</a></td>
                    <td><a href="/role/{{$item->id}}" class="text-dark">{{$item->cast->nama}}</a></td>
                    <td><a href="/role/{{$item->id}}" class="text-dark">{{$item->nama}}</a></td>           
                </tr> 
                    @empty
                        <h3>Data Tidak Ada</h3>
                    @endforelse
                </tbody>
            </table> 
        </div>          
    </div>
@auth
<a href="/role/create" class="btn btn-primary my-4">Submit</a>
@endauth
@endsection