@extends('layout.master')

@section('judul')
    Tambah Peran
@endsection

@section('content')

<div>
        <form action="/role" method="POST" enctype="multipart/form-data">
            @csrf
            <div>
                <label>Film</label>
                <select name="film_id" class="form-control" id="">
                    <option value="">--Pilih Film--</option>
                    @forelse ($film as $item)
                        <option value="{{$item->id}}">{{$item->judul}}</option>
                    @empty
                        <option value="">Tidak ada</option>
                    @endforelse
                </select>
                @error('film')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div>
                <label>Cast</label>
                <select name="cast_id" class="form-control" id="">
                    <option value="">--Pilih Cast--</option>
                    @forelse ($Cast as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @empty
                        <option value="">Tidak ada</option>
                    @endforelse
                </select>
                @error('cast')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Nama Peran</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>       
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection 