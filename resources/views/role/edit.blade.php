@extends('layout.master')

@section('judul')
    Tambah Peran
@endsection

@section('content')

<div>
        <form action="/role" method="POST" enctype="multipart/form-data">
            @csrf
            <div>
                <label>Film</label>
                <select name="film_id" class="form-control" id="">
                    <option value="">--Pilih Film--</option>
                    @forelse ($film as $item)
                        @if ($item->id === $role->film_id)
                            <option value={{$item->id}} selected>{{$item->judul}}</option>                            
                        @else
                            <option value={{$item->id}}>{{$item->judul}}</option>
                        @endif
                    @empty
                    <option value="">Film Tidak Ada</option>
                    @endforelse
                </select>
                @error('film')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div>
                <label>Cast</label>
                <select name="cast_id" class="form-control" id="">
                    <option value="">--Pilih Cast--</option>
                    @forelse ($cast as $item)
                        @if ($item->id === $role->cast_id)
                            <option value={{$item -> id}} selected>{{$item->nama}}</option>                            
                        @else
                            <option value={{$item -> id}}>{{$item->nama}}</option>
                        @endif
                    @empty
                    <option value="">Cast Tidak Ada</option>
                    @endforelse
                </select>
                @error('cast')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Nama Peran</label>
                <input type="text" value="{{$role->nama}}" class="form-control" name="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>       
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection 