@extends('layout.master')

@section('judul')
    Halaman Detail Role
@endsection

@section('content')

<div class="row">
    <div class="col-4">
        <div class="card">
            <div class="card-body" style="width: 200px">
                <label>Nama Film : {{$role->film->judul}}</label>
                <label>Nama Cast : {{$role->cast->nama}}</label>
                <label>Peran : {{$role->nama}}</label>
                @auth
                    <div class="d-flex justify-content-around my-1">
                        <form action="/role/{{$role->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/role/{{$role->id}}/edit" class="btn btn-secondary">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger">
                        </form>
                    </div>
                @endauth
            </div>
        </div>
    </div>
</div>

<a href="/role" class="btn btn-primary btn-sm">Kembali</a>
@endsection