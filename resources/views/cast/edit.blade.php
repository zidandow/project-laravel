@extends('layout.master')

@section('judul')
    Tambah  Edit Cast
@endsection

@section('content')

<div>
    <h2>Tambah Data</h2>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Nama Cast</label>
                <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="text" class="form-control" value="{{$cast->umur}}" name="umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Biodata</label>
                <textarea name="bio" class="form-control" id="" cols="30" rows="10" placeholder="Masukkan Biodata">{{$cast->bio}}</textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>           
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
</div>

@endsection 