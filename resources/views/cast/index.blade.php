@extends('layout.master')

@section('judul')
    Halaman Daftar Cast
@endsection

@section('content')

    <div class="row">
        @forelse ($cast as $key => $value)
            <div class="card mx-2" style="width: 15rem;">
                <div class="card-body">
                <h3 class="card-subtitle my-4">{{$value -> nama}}</h3>
                <a href="/cast/{{$value->id}}" class="btn btn-info btn-block">Detail</a>
                @auth
                    <div class="d-flex justify-content-around my-1">
                    <form action="/cast/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-secondary">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger">
                    </form>
                    </div>
                @endauth
                </div>
            </div>
        @empty
            <h3>Data Tidak Ada</h3>
        @endforelse
    </div>
@auth
    <a href="/cast/create" class="btn btn-primary my-4">Submit</a>
@endauth
@endsection