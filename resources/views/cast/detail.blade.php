@extends('layout.master')

@section('judul')
    Halaman Detail Cast
@endsection

@section('content')
    <h1>{{$cast->nama}}</h1>
    <h5>{{$cast->umur}} tahun</h5>
    <p>{{$cast->bio}}</p>

    <a href="/cast" class="btn btn-primary btn-sm">Kembali</a>
@endsection