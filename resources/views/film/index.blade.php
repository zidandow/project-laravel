@extends('layout.master')

@section('judul')
    Halaman Daftar Film
@endsection

@section('content')

    <div class="row">
        @forelse ($film as $key => $item)
        <div class="col-4">
            <div class="card" style="width: 15rem">
                <img src="{{asset('poster/'.$item->poster)}}" class="img-thumbnail card-img-top mw-100" alt="">
                <div class="card-body">
                    <h2 class="card-title">{{$item->judul}}</h2> <br>                    
                    <span class="badge badge-info">{{$item->genre->nama}}</span>
                    <p class="card-text">{{Str::limit($item->ringkasan, 55)}}</p>
                    <a href="/film/{{$item->id}}" class="btn btn-info btn-block">Detail</a>
                    @auth
                        <div class="d-flex justify-content-around my-1">
                            <form action="/film/{{$item->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/film/{{$item->id}}/edit" class="btn btn-secondary">Edit</a>
                                <input type="submit" value="Delete" class="btn btn-danger">
                            </form>
                        </div>
                    @endauth
                </div>
            </div>
        </div>
            
        @empty
            <h3>Data Tidak Ada</h3>
        @endforelse
    </div>
@auth
<a href="/film/create" class="btn btn-primary my-4">Submit</a>
@endauth
@endsection