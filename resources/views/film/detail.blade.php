@extends('layout.master')

@section('judul')
    Halaman Detail Film
@endsection

@section('content')

    <img src="{{asset('poster/'.$film->poster)}}" style="width: 150px" alt="">
    <h1>{{$film->judul}}</h1>
    <h5>{{$film->tahun}} tahun</h5>
    <p>{{$film->ringkasan}}</p>

    <a href="/role" class="btn btn-sm btn-primary">Cast</a>

    <hr>
    <h4>List Komentar</h4>
    @forelse ($film->Kritik as $item)
    <div class="card">
        <div class="card-header">
          {{$item->user->name}}
        </div>
        <div class="card-body">
          <h6 class="card-title">{{$item->point}}/10</h6>
          <p class="card-text">{{$item->content}}</p>
        </div>
      </div>
    @empty
        <h5>Tidak ada</h5>
    @endforelse

    @auth        
    <hr>
    <form action="/kritik/{{$film->id}}" method="POST" class="my-4">
        @csrf
        <h5>Kritik/Komentar</h5>
        <textarea name="content" id="" class="form-control" placeholder="Isi Kritik/Komentar disini"></textarea><br>
        <h5>Rating</h5>
        <input type="text" class="form-control" name="point" placeholder="Masukkan Rating Disini.. (1/10)"> <br>
        <input type="submit" value="Kirim Komentar" class="btn btn-primary btn-block">        

    </form>
    @endauth

    <a href="/film" class="btn btn-primary btn-sm">Kembali</a>
@endsection