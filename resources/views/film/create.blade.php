@extends('layout.master')

@section('judul')
    Tambah Film
@endsection

@section('content')

<div>
    <h2>Tambah Data</h2>
        <form action="/film" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Genre</label>
                <select name="genre_id" class="form-control" id="">
                    <option value="">-- Pilih Genre --</option>
                    @forelse ($genre as $item)
                    <option value="{{$item -> id}}">{{$item->nama}} </option>
                    @empty
                    <option value="">Genre Tidak Ada</option>
                    @endforelse
                </select>
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Judul Film</label>
                <input type="text" class="form-control" name="judul" placeholder="Masukkan Judul Film">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Ringkasan</label>
                <textarea name="ringkasan" class="form-control" id="" cols="30" rows="10" placeholder="Masukkan Ringkasan"></textarea>
                @error('ringkasan')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>   
            <div class="form-group">
                <label>Tahun</label>
                <input type="text" class="form-control" name="tahun" placeholder="Masukkan Tahun">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Poster</label>
                <input type="file" class="form-control" name="poster" placeholder="Masukkan Poster Film">
                @error('poster')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection 