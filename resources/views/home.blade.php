@extends('layout.master')

@section('judul')
    Zidan Film
@endsection

@section('content')  
<h2>Selamat Datang di Website Zidan Film</h2>
<p>Selamat datang di rumahnya para pecinta film! Di sini, kami membawa kehidupan pada layar Anda dengan koleksi terbaru dan klasik yang akan memikat imajinasi Anda. Nikmati pengalaman menonton tanpa batas dengan kualitas gambar yang memukau dan pilihan yang tak tertandingi. Segera temukan dunia yang penuh warna ini dan biarkan kami menjadi pengantar Anda dalam setiap petualangan sinematik. Selamat menikmati!</p>

<a href="/film" class="btn btn-primary btn-block" style="width: 100%">Klik disini untuk melihat daftar Film</a>
@endsection