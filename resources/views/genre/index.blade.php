@extends('layout.master')

@section('judul')
    Daftar Genre
@endsection

@section('content')

    <div class="row">
        @forelse ($genre as $key => $value)
            <div class="card mx-2" style="width: 15rem;">
                <div class="card-body">
                <h6 class="card-title">{{$key + 1}}</h6>
                <h4 class="card-subtitle my-4">{{$value -> nama}}</h4>                
                <a href="/genre/{{$value->id}}" class="btn btn-info btn-block">Detail</a>
                @auth                
                <div class="d-flex justify-content-around my-1">                    
                <form action="/genre/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/genre/{{$value->id}}/edit" class="btn btn-secondary">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger">
                </form>
                </div>
                @endauth
                </div>
            </div>
        @empty
            <h3>Data Tidak Ada</h3>
        @endforelse
    </div>
@auth
<a href="/genre/create" class="btn btn-primary my-4">Submit</a>
@endauth
@endsection