@extends('layout.master')

@section('judul')
    Edit Genre
@endsection

@section('content')

<div>
    <h2>Tambah Data</h2>
        <form action="/genre/{{$genre->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Nama Genre</label>
                <input type="text" class="form-control" value="{{$genre->nama}}" name="nama" placeholder="Masukkan Nama Genre">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>         
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
</div>

@endsection 