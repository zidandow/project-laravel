@extends('layout.master')

@section('judul')
    Halaman Detail Genre
@endsection

@section('content')

    <h1>{{$genre->nama}}</h1>

    @forelse ($genre->listFilm as $item)
    <div class="col-4">
        <div class="card" style="width: 15rem">
            <img src="{{asset('poster/'.$item->poster)}}" class="img-thumbnail card-img-top mw-100" alt="">
            <div class="card-body">
                <h2 class="card-title">{{$item->judul}}</h2>
                <p class="card-text">{{Str::limit($item->ringkasan, 55)}}</p>
                <a href="/film/{{$item->id}}" class="btn btn-info btn-block">Detail</a>
            </div>
        </div>
    </div>
    @empty
        <h4>Tidak Ada Film di Genre ini.</h4>
    @endforelse

    <a href="/genre" class="btn btn-primary btn-sm">Kembali</a>
@endsection