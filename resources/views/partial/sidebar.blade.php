<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{asset('/AdminLTE-3.2.0/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        @auth
        <a href="/login" class="d-block">{{Auth::User()->name}}</a>
            
        @endauth

        @guest
        
        <a href="/login" class="d-block">Silahkan Login</a>
        @endguest
      </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="/" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="/cast" class="nav-link">
            <i class="nav-icon fas fa-list"></i>
            <p>
              Cast
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="/film" class="nav-link">
            <i class="nav-icon fas fa-film"></i>
            <p>
              Film
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="/genre" class="nav-link">
            <i class="nav-icon fas fa-masks-theater"></i>
            <p>
              Genre
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="/role" class="nav-link">
            <i class="nav-icon fas fa-person-square"></i>
            <p>
              Peran
            </p>
          </a>
        </li>

        @auth        
        <li class="nav-item bg-danger">
          <a class="nav-link" href="{{ route('logout') }}"
             onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
              Logout
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
              @csrf
          </form>
        </li>
        @endauth

        @guest
        <li class="nav-item bg-primary">
          <a href="/login" class="nav-link">
            <i class="nav-icon fas fa-user"></i>
            <p>
              Login
            </p>
          </a>
        </li>
        @endguest
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>